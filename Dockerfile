FROM python:3.6-buster

WORKDIR /app

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

RUN pip3 install --upgrade pip
RUN pip3 install Flask gunicorn

COPY . /app
